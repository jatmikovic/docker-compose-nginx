#!/bin/bash
acr_url="registry.gitlab.com"
acr_namespace="jatmikovic"
acr_repo_name="docker-compose-nginx"
version_file="version.txt"
increment="0.01"

# Get the current branch name from Git without the 'dev' prefix
current_branch=$(git rev-parse --abbrev-ref HEAD | sed 's/^dev//')

# Check if the version file exists; if not, create it with an initial version
if [ ! -f "$version_file" ]; then
    echo "${current_branch}-00.00.00" > "$version_file"
fi

# Read the current version from the file
tag_version=$(cat "$version_file")

# Extract major, minor, and build parts
IFS='.-' read -ra version_parts <<< "$tag_version"
major="${version_parts[1]}"
minor="${version_parts[2]}"
build="${version_parts[3]}"

# Increment the build version by 1 and ensure it's two digits
build=$((10#$build + 1))
build=$(printf "%02d" $build)

# Format the next version
next_version="${current_branch}-${major}.${minor}.${build}"

# Update the version in the file
echo "$next_version" > "$version_file"

docker build -t ${acr_url}/${acr_namespace}/${acr_repo_name}:${next_version} .
sleep 2
docker push ${acr_url}/${acr_namespace}/${acr_repo_name}:${next_version}
